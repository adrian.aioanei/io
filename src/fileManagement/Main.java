package fileManagement;

import java.io.File;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;

public class Main {
    // Adrian Aioanei
	// In a real application we should take in consideration 
	// a 32 processor bit architecture. I dont't know if there are persons
	// that use them but you will use a 32 bit processor and a file larger then
	// 2^32 the application will crush
	
	public static void main(String[] args) {
		CharBuffer charBuffer = CharBuffer.wrap(".........adrian................");
		
		//default param
		WriteContentToFile defaultParam = new WriteContentToFile("test1.txt");
		defaultParam.test(charBuffer);
		
		//truncate
		WriteContentToFile setFileOptions = new WriteContentToFile("test2.txt",StandardOpenOption.TRUNCATE_EXISTING);
		setFileOptions.test(charBuffer);
		
		//append and utf-16 encooding
		WriteContentToFile setEncoding = new WriteContentToFile("test3.txt",StandardOpenOption.APPEND,Charset.forName("utf-16"));
		setEncoding.test(charBuffer);
	}
}
