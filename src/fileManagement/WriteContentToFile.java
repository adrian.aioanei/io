package fileManagement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class WriteContentToFile {
	private String fileName                   = null;
	private StandardOpenOption openOption     = null;
	private Charset format 				      = null;
	private MappedByteBuffer mappedByteBuffer = null;

	//set all the parameters
	public WriteContentToFile(String fileName,StandardOpenOption openOption,Charset format) {
		this.fileName = fileName;
		this.openOption = openOption;
		this.format = format;
	}

	//default options
	public WriteContentToFile(String fileName,StandardOpenOption openOption) {
		this.fileName = fileName;
		this.openOption = openOption;
		this.format = Charset.forName("utf-8");
	}

	//default options
	public WriteContentToFile(String fileName) {
		this.fileName = fileName;
		this.openOption = StandardOpenOption.APPEND;
		this.format = Charset.forName("utf-8");
	}

	public Path getFileURIFromResources(String fileName) {
		return Paths.get(fileName);
	}

	private long getFileSize() {
		return new File(fileName).length();
	}

	private void cleanFile() {
		try {
			File f = new File(fileName);
			RandomAccessFile raf = new RandomAccessFile(f, "rw");
			raf.setLength(0);
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void mapFile(int length) {
		Path pathToWrite;
		long offset = 0;

		pathToWrite = getFileURIFromResources(fileName);

		if(openOption.compareTo(StandardOpenOption.APPEND) == 0)
			offset = getFileSize();
		else
			cleanFile();

		try {
			FileChannel fileChannel = 
					(FileChannel) 
					Files.newByteChannel(pathToWrite, EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE));

			mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, offset, length);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void test(CharBuffer charBuffer) {
		//we should map a bigger array in order to avoid multiple maps
		mapFile(charBuffer.length());

		if (mappedByteBuffer != null) 
			mappedByteBuffer.put(format.encode(charBuffer));
	}
}